import React, { Component } from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { authenticationService } from "../../services";

class Login extends Component {
  constructor(props) {
    super(props);
    // redirect to home if already logged in
    if (authenticationService.currentUserValue) {
      this.props.history.push("/");
    }
  }

  render() {
    return (
      <React.Fragment>
        <Formik
          initialValues={{
            username: "",
            password: ""
          }}
          validationSchema={Yup.object().shape({
            username: Yup.string().required("Usuario es requerido"),
            password: Yup.string().required("Contraseña es requerido")
          })}
          onSubmit={({ username, password }, { setStatus, setSubmitting }) => {
            setStatus();
            authenticationService.login(username, password).then(
              user => {
                const { from } = this.props.location.state || {
                  from: { pathname: "/" }
                };
                this.props.history.push(from);
              },
              error => {
                setSubmitting(false);
                setStatus(error);
              }
            );
          }}
          render={({ errors, status, touched, isSubmitting }) => (
            <div className="auth">
              <div className="auth-container">
                <div className="auth-box">
                  <ul className="auth-Tablist">
                    <li className="auth-title">Iniciar Sesion</li>
                  </ul>
                  <div className="tabpanel">
                    <Form className="SignForm">
                      <div className="SignForm_element">
                        <Field
                          name="username"
                          type="text"
                          placeholder="Usuario"
                          className={
                            "form-control SignForm_input" +
                            (errors.username && touched.username
                              ? " is-invalid"
                              : "")
                          }
                        />
                        <ErrorMessage
                          name="username"
                          component="div"
                          className="invalid-feedback"
                        />
                      </div>
                      <div className="SignForm_element">
                        <Field
                          name="password"
                          type="password"
                          placeholder="Contraseña"
                          className={
                            "form-control SignForm_input" +
                            (errors.password && touched.password
                              ? " is-invalid"
                              : "")
                          }
                        />
                        <ErrorMessage
                          name="password"
                          component="div"
                          className="invalid-feedback"
                        />
                      </div>
                      <button
                        type="submit"
                        className="SignForm-login-button"
                        disabled={isSubmitting}
                      >
                        <span>Iniciar Sesion</span>
                      </button>
                    </Form>
                  </div>
                </div>
              </div>
            </div>
          )}
        />
      </React.Fragment>
    );
  }
}

export default Login;
