import React from "react";
import { connect } from "react-redux";
import { formatMoney } from "../../pipes/priceFormatter";
import CartItem from "../../components/CartItem/CartItem";
import CreditCard from "../../components/CreditCard/CreditCard";
const credit = {
  num: [5310, 4356, 7854, 3801],
  exp: "04/22",
  user: "mr Javier A. Segovia"
};

const ShoppingCart = props => {
  return (
    <div className="container content">
      <CreditCard credit={credit} />
      <div className="title-cart-container">
        <h1 className="title-cart">Carrito</h1>
      </div>
      <div className="card shopping-cart">
        <div className="card-body">
          {props.cartItemCount ? (
            props.cartItems.map(cart => (
              <CartItem {...cart} img={cart.images[0]} />
            ))
          ) : (
            <h1 className="display-4 mt-5 text-center">
              No hay producto en tu carrito.
            </h1>
          )}
        </div>
        <div className="card-footer">
          <div className="pull-right" style={{ margin: "10px" }}>
            <div className="pull-right" style={{ margin: "5px" }}>
              <h1 className="title-price">
                Total Precio:$ <b>{formatMoney(props.totalPrice)}</b>
              </h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  console.log(state, "state has changed");

  return {
    cartItems: state.shop.cart,
    cartItemCount: state.shop.cart.reduce((count, curItem) => {
      return count + curItem.quantity;
    }, 0),
    totalPrice: state.shop.cart.reduce((count, curItem) => {
      return count + curItem.price * curItem.quantity;
    }, 0)
  };
};

export default connect(
  mapStateToProps,
  null
)(ShoppingCart);
