import { combineReducers } from "redux";
import shop from "./shop";
import { brandFilterReducer } from "./brand";
import { orderByPriceReducer } from "./orderByPrice";
import { paginationReducer } from "./pagination";

export default combineReducers({
  shop,
  brandFilter: brandFilterReducer,
  orderBy: orderByPriceReducer,
  pagination: paginationReducer
});
