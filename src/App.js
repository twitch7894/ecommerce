import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom";
import { history } from "./helpers";
import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "./reducers";
import { authenticationService } from "./services";
import PrivateRoute from "./components/PrivateRoute";
import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login";
import posed from 'react-pose';
import Header from "./components/Header/Header";
import ProductDetail from "./pages/ProductDetail/ProductDetail";
import ShoppingCart from "./pages/ShopingCart/ShoppingCart";

export const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
const RouteContainer = posed.div({
  enter: { opacity: 1, delay: 300, beforeChildren: true },
  exit: { opacity: 0 }
});

class App extends Component {
  state = {
    currentUser: null
  };
  componentDidMount() {
    authenticationService.currentUser.subscribe(x =>
      this.setState({ currentUser: x })
    );
  }
  logout() {
    authenticationService.logout();
    history.push("/login");
  }
  

  render() {
    const { currentUser } = this.state;
    console.log(currentUser);
    return (
      <Provider store={store}>
        <Router history={history}>
          <React.Fragment>
            {currentUser && <Header logout={this.logout} />}
            <Switch>
              <Route path="/login" component={Login} />
              <Route
                exact
                path={"/"}
                render={() => {
                  return <Redirect to={"/products"} />;
                }}
              />
              <PrivateRoute exact path="/products" component={Home} />
              <Route  path="/products/:id" component={ProductDetail} />
              <Route  path="/cart" component={ShoppingCart} />
            </Switch>
          </React.Fragment>
        </Router>
      </Provider>
    );
  }
}

export default App;
