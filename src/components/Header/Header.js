import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
const logouted = `https://cdn1.iconfinder.com/data/icons/interface-elements-ii-1/512/Logout-512.png`;
const cart = `http://www.pngmart.com/files/7/Cart-Background-PNG.png`;

const Header = ({ cartLength, logout }) => {
  return (
    <React.Fragment>
      <header className="Container-Header">
        <div className="Header">
          <div className="logo">
            <NavLink to="/">
              <img
                src={`https://upload.wikimedia.org/wikipedia/commons/3/31/Logo_Banco_Galicia.svg`}
                alt="Banco Galicia"
                width={150}
              />
            </NavLink>
          </div>
          <div className="Header_nav">
            <ul className="Navs">
              <li className="li-nav">
                <NavLink className="nav_links" to="/">
                  Celulares
                </NavLink>
              </li>
              <li className="li-nav">
                <a
                  className="nav_links"
                  href="https://gitlab.com/twitch7894/ecommerce"
                >
                  Repo Gitlab
                </a>
              </li>
            </ul>
          </div>
          <div className="Header-Icon">
            <NavLink className="Header-Icon-cart" to={"/cart"}>
              <div className="Header-cart">
                <span className="Cart-number">{cartLength}</span>
                <img src={cart} alt="cart" width="32" />
              </div>
            </NavLink>
            <NavLink className="Header-Icon-cart" onClick={logout} to={"/login"}>
              <div className="Header-logout">
                <img src={logouted} alt="logout" width="22" />
              </div>
            </NavLink>
          </div>
        </div>
      </header>
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    cartLength: state.shop.cart.length
  };
};

export default connect(
  mapStateToProps,
  null
)(Header);
