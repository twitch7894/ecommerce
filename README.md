<div align="center">
  <a href="https://github.com/babel/babel">
    <img width="100"  src="https://upload.wikimedia.org/wikipedia/commons/0/02/Babel_Logo.svg">
  </a>
  <a href="https://github.com/webpack/webpack">
    <img width="100" src="https://webpack.js.org/assets/icon-square-big.svg">
  </a>

  <a href="https://prettier.io/">
    <img width="100" src="https://d11a6trkgmumsb.cloudfront.net/original/3X/5/c/5cf638850999b71ae3c48a4aa5031c4a25473ef0.png">
  </a>

  <a href="https://github.com/TrackJs">
    <img width="100" src="https://static.crozdesk.com/web_app_library/providers/logos/000/004/129/original/trackjs-1522094349-logo.png?1522094349">
  </a>

</div>
  <br>
  <br>


## Quick Overview

```sh
git clone https://gitlab.com/twitch7894/ecommerce
cd ecommerce
npm install o yarn
```
```sh
Login: username: `admin`, password: `admin`
```
Then open [http://localhost:800/](http://localhost:8080/) to see your app.<br>
When you’re ready to deploy to production, create a minified bundle with `yarn prod`.
when you’re ready to deploy to development, bundle with `yarn dev`.